// const { watch, reload } = require("browser-sync");

const gulp = require("gulp"),
  sass = require("gulp-sass"),
  browserSync = require("browser-sync").create(),
  minifyjs = require("gulp-minify"),
  uglify = require("gulp-uglify"),
  clean = require("gulp-clean"),
  cleanCSS = require("gulp-clean-css"),
  concat = require("gulp-concat"),
  imagemin = require("gulp-imagemin"),
  autoprefixer = require("gulp-autoprefixer");

const cleanDist = () => {
  return gulp.src("dist", { read: false }).pipe(clean());
};

const scssBuild = () => {
  return gulp
    .src("src/scss/*.scss")
    .pipe(sass())
    .pipe(concat("main.css"))
    .pipe(autoprefixer({ cascade: false }))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(gulp.dest("dist/css/"));
};

const jsBuild = () => {
  return gulp
    .src("src/js/*.js")
    .pipe(concat("main.js"))
    .pipe(minifyjs())
    .pipe(uglify())
    .pipe(gulp.dest("dist/js/"));
};

const imagesBuild = () => {
  return gulp
    .src("src/img/*")
    .pipe(
      imagemin({
        interlaced: true,
        progressive: true,
        optimizationLevel: 5,
      })
    )
    .pipe(gulp.dest("dist/img/"));
};

const watch = () => {
  gulp.watch("src/scss/*.scss", scssBuild).on("change", browserSync.reload);
  gulp.watch("src/js/*.js", jsBuild).on("change", browserSync.reload);
  gulp.watch("./index.html").on("change", browserSync.reload);
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
};

gulp.task("dev", watch);
gulp.task("build", gulp.series(cleanDist, scssBuild, jsBuild, imagesBuild));
